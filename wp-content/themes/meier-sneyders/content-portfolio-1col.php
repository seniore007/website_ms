<?php
/**
 * The template to display post content
 *
 * @package Agency
 * @since 1.0
 */

$theme_options = get_theme_mod('zilla_theme_options');

$terms = get_the_terms( $post->ID, 'portfolio-type' );
$term_list = '';
if( !empty($terms) ) {
	foreach( $terms as $term ) {
		$term_list .= 'term-'. $term->slug .' ';
	}
	$term_list = trim($term_list);
}

zilla_post_before(); ?>
<!--BEGIN .post -->
<article id="post-<?php the_ID(); ?>" <?php post_class( $term_list .' type-portfolio-1col' ); ?>>
<?php zilla_post_start(); ?>

	<?php if ( has_post_thumbnail() ) { ?>
		<div class="entry-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'portfolio-thumb' ); ?>
				<div class="overlay"></div>
			</a>
		</div>
	<?php } ?>

	<!--BEGIN .entry-header-->
	<header class="entry-header">

		<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

		<!--BEGIN .entry-summary -->
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		<!--END .entry-summary -->
		</div>

		<a href="<?php the_permalink(); ?>" class="btn"><?php _e( 'View Project', 'zilla' ) ?></a>

	<!--END .entry-header-->
	</header>

<?php zilla_post_end(); ?>
<!--END .post-->
</article>
<?php zilla_post_after(); ?>
