<?php
/**
 * The template to display post content
 *
 * @package Agency
 * @since 1.0
 */

$theme_options = get_theme_mod('zilla_theme_options');

$terms = get_the_terms( $post->ID, 'portfolio-type' );
$term_list = '';
if( !empty($terms) ) {
	foreach( $terms as $term ) {
		$term_list .= 'term-'. $term->slug .' ';
	}
	$term_list = trim($term_list);
}

zilla_post_before(); ?>
<!--BEGIN .post -->
<article id="post-<?php the_ID(); ?>" <?php post_class( $term_list .' type-portfolio-2col' ); ?>>
<?php zilla_post_start(); ?>

	<?php if ( has_post_thumbnail() ) { ?>
		<div class="entry-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'portfolio-thumb-small' ); ?>
				<h2 class="entry-title"><span class="cell"><span><?php the_title(); ?></span></span></h2>
			</a>
		</div>
	<?php } ?>

<?php zilla_post_end(); ?>
<!--END .post-->
</article>
<?php zilla_post_after(); ?>
