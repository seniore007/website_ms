<?php
/**
 * Template Name: About
 *
 * A custom About page template
 *
 * @package Agency
 * @since 1.0
 */

get_header(); ?>

	<!--BEGIN #primary .site-main-->
	<div id="primary" class="site-main full-width" role="main">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php zilla_page_before(); ?>
		<!--BEGIN .page-->
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
		<?php zilla_page_start(); ?>

			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>

	            <?php if ( current_user_can( 'edit_post', $post->ID ) ): ?>
					<!--BEGIN .entry-meta-->
					<div class="entry-meta">
						<?php edit_post_link( __('Edit', 'zilla'), '<span class="edit-post">', '</span>' ); ?>
					<!--END .entry-meta-->
					</div>
				<?php endif; ?>

			</header>

			<!--BEGIN .entry-content -->
			<div class="entry-content">
				<?php the_content(__('Read more...', 'zilla')); ?>
				<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'zilla').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			<!--END .entry-content -->
			</div>

			<div class="entry-profiles clearfix">
				<?php
				for( $i = 1; $i <= 12; $i++ ){
					$image = get_post_meta( $post->ID, '_zilla_about_image_'. $i, true );
					$name = get_post_meta( $post->ID, '_zilla_about_name_'. $i, true );
					$title = get_post_meta( $post->ID, '_zilla_about_title_'. $i, true );
					$bio = get_post_meta( $post->ID, '_zilla_about_bio_'. $i, true );
					$twitter = get_post_meta( $post->ID, '_zilla_about_twitter_'. $i, true );
					$dribbble = get_post_meta( $post->ID, '_zilla_about_dribbble_'. $i, true );
					$linkedin = get_post_meta( $post->ID, '_zilla_about_linkedin_'. $i, true );
					$behance = get_post_meta( $post->ID, '_zilla_about_behance_'. $i, true );
					$medium = get_post_meta( $post->ID, '_zilla_about_medium_'. $i, true );
					if($image && $name){
						?>
						<div class="profile">
							<img src="<?php echo $image; ?>" alt="<?php echo $name; ?>" class="profile-image">
							<h3 class="profile-name"><?php echo $name; ?></h3>
							<p class="profile-title"><?php echo $title; ?></p>
							<p class="profile-bio"><?php echo $bio; ?></p>
							<div class="profile-links">
								<?php if( $twitter ){ ?><a href="<?php echo $twitter ?>" class="twitter"><?php include( get_template_directory() .'/images/twitter-icon.svg' ); ?></a><?php } ?>
								<?php if( $dribbble ){ ?><a href="<?php echo $dribbble ?>" class="dribbble"><?php include( get_template_directory() .'/images/dribbble-icon.svg' ); ?></a><?php } ?>
								<?php if( $linkedin ){ ?><a href="<?php echo $linkedin ?>" class="linkedin"><?php include( get_template_directory() .'/images/linkedin-icon.svg' ); ?></a><?php } ?>
								<?php if( $behance ){ ?><a href="<?php echo $behance ?>" class="behance"><?php include( get_template_directory() .'/images/behance-icon.svg' ); ?></a><?php } ?>
								<?php if( $medium ){ ?><a href="<?php echo $medium ?>" class="medium"><?php include( get_template_directory() .'/images/medium-icon.svg' ); ?></a><?php } ?>
							</div>
						</div>
						<?php
					}
				}
				?>
			</div>

		<?php zilla_page_end(); ?>
		<!--END .page-->
		</article>
		<?php zilla_page_after(); ?>

		<?php endwhile; endif; ?>

	<!--END #primary .site-main-->
	</div>

<?php get_footer(); ?>
