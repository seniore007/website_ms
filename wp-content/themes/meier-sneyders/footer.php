<?php
/**
 * The template for showing our footer
 *
 * @package Agency
 * @since 1.0
 */

$theme_options = get_theme_mod('zilla_theme_options');
?>

		<?php zilla_content_end(); ?>
		<!-- END #content .site-content-->
		</div>

		<?php zilla_footer_before(); ?>
		<!-- BEGIN #footer -->
		<footer id="footer" class="site-footer" role="contentinfo">
		<?php zilla_footer_start(); ?>

			<p class="credit">
				<span class="copyright">&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></span>
				<span class="theme-by"><?php _e('Agency Theme by', 'zilla') ?> <a href="http://www.themezilla.com">ThemeZilla</a></span>
				<span class="powered-by"><?php _e('Powered by', 'zilla') ?> <a href="http://wordpress.org">WordPress</a></span>
			</p>

			<div class="social">
				<?php
				if( isset($theme_options['facebook_url']) && $theme_options['facebook_url'] ){ echo '<a href="'. filter_var( $theme_options['facebook_url'], FILTER_SANITIZE_URL ) .'" class="facebook" title="Follow on Facebook">'; include( get_template_directory() .'/images/facebook-icon.svg' ); echo '</a>'; }
				if( isset($theme_options['twitter_url']) && $theme_options['twitter_url'] ){ echo '<a href="'. filter_var( $theme_options['twitter_url'], FILTER_SANITIZE_URL ) .'" class="twitter" title="Follow on Twitter">'; include( get_template_directory() .'/images/twitter-icon.svg' ); echo '</a>'; }
				if( isset($theme_options['linkedin_url']) && $theme_options['linkedin_url'] ){ echo '<a href="'. filter_var( $theme_options['linkedin_url'], FILTER_SANITIZE_URL ) .'" class="linkedin" title="Follow on LinkedIn">'; include( get_template_directory() .'/images/linkedin-icon.svg' ); echo '</a>'; }
				if( isset($theme_options['behance_url']) && $theme_options['behance_url'] ){ echo '<a href="'. filter_var( $theme_options['behance_url'], FILTER_SANITIZE_URL ) .'" class="behance" title="Follow on Behance">'; include( get_template_directory() .'/images/behance-icon.svg' ); echo '</a>'; }
				if( isset($theme_options['dribbble_url']) && $theme_options['dribbble_url'] ){ echo '<a href="'. filter_var( $theme_options['dribbble_url'], FILTER_SANITIZE_URL ) .'" class="dribbble" title="Follow on Dribbble">'; include( get_template_directory() .'/images/dribbble-icon.svg' ); echo '</a>'; }
				if( isset($theme_options['pinterest_url']) && $theme_options['pinterest_url'] ){ echo '<a href="'. filter_var( $theme_options['pinterest_url'], FILTER_SANITIZE_URL ) .'" class="pinterest" title="Follow on Pinterest">'; include( get_template_directory() .'/images/pinterest-icon.svg' ); echo '</a>'; }
				if( isset($theme_options['medium_url']) && $theme_options['medium_url'] ){ echo '<a href="'. filter_var( $theme_options['medium_url'], FILTER_SANITIZE_URL ) .'" class="medium" title="Follow on Medium">'; include( get_template_directory() .'/images/medium-icon.svg' ); echo '</a>'; }
				if( isset($theme_options['tumblr_url']) && $theme_options['tumblr_url'] ){ echo '<a href="'. filter_var( $theme_options['tumblr_url'], FILTER_SANITIZE_URL ) .'" class="tumblr" title="Follow on Tumblr">'; include( get_template_directory() .'/images/tumblr-icon.svg' ); echo '</a>'; }
				?>
			</div>

		<?php zilla_footer_end(); ?>
		<!-- END #footer -->
		</footer>
		<?php zilla_footer_after(); ?>

	<!-- END #container .hfeed .site -->
	</div>

	<!-- Theme Hook -->
	<?php wp_footer(); ?>
	<?php zilla_body_end(); ?>

	<!-- <?php echo 'Ran '. $wpdb->num_queries .' queries '. timer_stop(0, 2) .' seconds'; ?> -->
<!--END body-->
</body>
<!--END html-->
</html>
