<?php
/**
 * The template for displaying the portfolio type archives.
 *
 * @package Agency
 * @since 1.0
 */

get_header();
?>

	<!--BEGIN #primary .site-main-->
	<div id="primary" class="site-main" role="main">
		<?php
		$terms = get_terms( 'portfolio-type', array('hierarchical' => false) );
		if( count($terms) ){
			echo '<ul class="portfolio-type-nav">';
			//echo '<li><a href="#" data-filter="*">'. __( 'All', 'zilla' ) .'</a> / </li>';
			foreach( $terms as $term ) {
				$output = '<li><a href="'. get_term_link($term) .'" data-filter=".term-'. $term->slug .'"'. (is_tax( 'portfolio-type', $term ) ? ' class="active"' : '') .'>'. $term->name .'</a>';
				if( $term !== end($terms) ) $output .= ' / ';
				echo $output .'</li>';
			}
			echo '</ul>';
		}
		?>

		<?php if( have_posts() ) : ?>

			<?php while( have_posts() ) : the_post(); ?>

				<?php get_template_part('content', 'portfolio-1col' ); ?>

			<?php endwhile; ?>

		<?php endif; ?>
	<!--END #primary .site-main-->
	</div>

<?php get_footer(); ?>
