/*-----------------------------------------------------------------------------------

 	Custom JS - All front-end jQuery

-----------------------------------------------------------------------------------*/

;(function($) {
	"use strict";

	$(document).ready(function($) {

		/* --------------------------------------- */
		/* ZillaMobileMenu & Superfish
		/* --------------------------------------- */
		$('#primary-menu')
			.zillaMobileMenu({
				breakPoint: 768,
				hideNavParent: true,
				prependTo: '.site-header',
				speed: 300,
				onInit: function( menu ) {
					$(menu).removeClass('zilla-sf-menu primary-menu');
				}
			})
			.superfish({
		    		delay: 200,
		    		animation: {opacity:'show', height:'show'},
		    		speed: 'fast',
		    		cssArrows: false,
		    		disableHI: true
			});

		/* --------------------------------------- */
		/* jPlayer - Audio/Video Media
		/* --------------------------------------- */
		if( $().jPlayer ) {
			var $jplayers = $('.jp-jplayer');

			$jplayers.each(function() {
				var $player = $(this),
					playerType = $player.data('player-type'),
					playerMedia = $player.data('media-info');

				if( playerType === 'video' ) {
					$player.jPlayer({
						ready: function() {
							$(this).jPlayer('setMedia', {
								poster: playerMedia.p,
								m4v: playerMedia.m,
								ogv: playerMedia.o,
							});
						},
						size: {
							width: '100%',
							height: 'auto',
						},
						play: function() { // To avoid multiple jPlayers playing together.
							$(this).jPlayer("pauseOthers");
						},
						swfPath: zillaStatesman.jsFolder,
						cssSelectorAncestor: playerMedia.ancestor,
						supplied: 'm4v, ogv'
					});

					// Show/Hide player controls when video playing
					$player.bind($.jPlayer.event.playing, function(e) {
						var gui = $(this).next('.jp-video').find('.jp-interface');
						$(this).add(gui).hover( function() {
							$(gui).stop().animate({ opacity: 1 }, 300);
						}, function() {
							$(gui).stop().animate({ opacity: 0}, 300);
						});
					});

					$player.bind($.jPlayer.event.pause, function(e) {
						var gui = $(this).next('.jp-video').find('.jp-interface');
						$(this).add(gui).unbind('hover');
						$(gui).stop().animate({ opacity: 1 }, 300);
					});
				} else {
					$player.jPlayer({
						ready: function() {
							$(this).jPlayer('setMedia', {
								poster: playerMedia.p,
								mp3: playerMedia.m,
								oga: playerMedia.o,
							});
						},
						size: {
							width: '100%',
							height: 'auto',
						},
						play: function() { // To avoid multiple jPlayers playing together.
							$(this).jPlayer("pauseOthers");
						},
						preload: 'auto',
						swfPath: zillaStatesman.jsFolder,
						cssSelectorAncestor: playerMedia.ancestor,
						supplied: 'mp3, ogg'
					});
				}
			});
		} /* jPlayer --- */

		/* --------------------------------------- */
		/* Responsive media - FitVids
		/* --------------------------------------- */
		if( $().fitVids ) {
			$('#content').fitVids();
		} /* FitVids --- */

        /* ------------------------------------------------- */
        /* Isotope
        /* ------------------------------------------------- */
        if( $().isotope ) {
            var $container = $('.portfolio-container');

            if($container.length){
				$container.imagesLoaded(function(){
					$container.isotope({
						itemSelector: '.type-portfolio',
						layoutMode: 'fitRows',
						hiddenStyle: {
							opacity: 0
						},
						visibleStyle: {
							opacity: 1
						}
					});
				});

                $('.portfolio-type-nav a').on('click', function(e){
                    e.preventDefault();
                    $container.isotope({
                        filter: $(this).attr('data-filter')
                    });

					$('.portfolio-type-nav a').removeClass('active');
					$(this).addClass('active');
                });
            }
        }

		/* ------------------------------------------------- */
		/* Home Slider
		/* ------------------------------------------------- */
		if( $().slidesjs ) {
			var $slider = $('.home-slider');
			if($slider.length){
				var sliderHeight = $(window).height() - $('.site-header').outerHeight();
				// Cheat target iPhone landscape
				if($(window).width() <= 500 || $(window).width() >= 600){
					sliderHeight -= $('.site-footer').outerHeight();
				}

				$slider.imagesLoaded(function(){
					$slider.slidesjs({
						width: $('.site-content').width(),
						height: sliderHeight,
						pagination: {
							active: false
						}
					});
				});
			}
		}

		/* ------------------------------------------------- */
		/* Gallery Slider
		/* ------------------------------------------------- */
		if( $().slidesjs ) {
			var $slideshows = $('.slideshow');
			if($slideshows.length){
				$slideshows.each(function(){
					var $slider = $(this);
					$slider.imagesLoaded(function(){
						$slider.slidesjs({
							width: $slider.width(),
							height: $slider.find('img').height(),
							pagination: {
								active: false
							}
						});
					});
				});
			}
		}

		/* ------------------------------------------------- */
		/* Portfolios
		/* ------------------------------------------------- */

		if($('.page-template-template-portfolio-2col-php').length){
			$('.page-template-template-portfolio-2col-php .portfolio-container').imagesLoaded(function(){
				$('.type-portfolio-2col,.type-portfolio-2col .entry-title').height($('.type-portfolio-2col .entry-thumbnail img').height());
			});

			$(window).resize(function(){
				$('.type-portfolio-2col,.type-portfolio-2col .entry-title').height($('.type-portfolio-2col .entry-thumbnail img').height());
			});
		}

	});

})(window.jQuery);
