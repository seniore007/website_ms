<?php
/**
 * The template to display post content
 *
 * @package Agency
 * @since 1.0
 */

zilla_post_before(); ?>
<!--BEGIN .post -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php zilla_post_start(); ?>

	<!--BEGIN .entry-header-->
	<header class="entry-header">

		<?php if( is_single() ) { ?>
			<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php } else { ?>
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php } ?>

		<?php if ( has_post_thumbnail() ) { ?>
			<div class="entry-thumbnail">
				<a title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'zilla' ), the_title_attribute( 'echo=0' ) ) ); ?>" href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail(); ?>
				</a>
			</div>
		<?php } ?>

		<?php zilla_post_meta_header(); ?>

	<!--END .entry-header-->
	</header>

	<?php agency_the_content(); ?>

	<footer class="entry-footer">
		<?php zilla_post_meta_footer(); ?>
	</footer>

<?php zilla_post_end(); ?>
<!--END .post-->
</article>
<?php zilla_post_after(); ?>
