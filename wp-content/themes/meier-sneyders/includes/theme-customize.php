<?php
/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since Broadcast 1.0
 */

add_action('customize_register', 'zilla_customize_register');
function zilla_customize_register($wp_customize) {

  class Zilla_Customize_Textarea_Control extends WP_Customize_Control {
    public $type = 'textarea';

    public function render_content() {
      ?>
      <label>
        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
        <textarea style="width:100%" rows="8" <?php $this->link(); ?>><<?php echo esc_textarea( $this->value() ); ?></textarea>
      </label>
      <?php
    }
  }

  /* General Options --- */
  $wp_customize->add_section(
    'zilla_general_options',
     array(
        'title' => __( 'General Options', 'zilla' ),
        'priority' => 10,
        'capability' => 'edit_theme_options',
        'description' => __('Control and configure the general setup of your theme.', 'zilla')
     )
  );

  $wp_customize->add_setting(
    'zilla_theme_options[general_text_logo]',
    array( 'default' => '0' )
  );

  $wp_customize->add_control( 'zilla_general_text_logo', array(
    'label' => __( 'Plain Text Logo', 'zilla' ),
    'section' => 'zilla_general_options',
    'settings' => 'zilla_theme_options[general_text_logo]',
    'type' => 'checkbox'
  ));

  $wp_customize->add_setting(
    'zilla_theme_options[general_custom_logo]',
    array(
      'default' => get_template_directory_uri() . '/images/logo.png',
      'transport' => 'postMessage'
    )
  );

  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'zilla_general_custom_logo',
    array(
      'label' => __( 'Logo Upload', 'zilla' ),
      'section' => 'zilla_general_options',
      'settings' => 'zilla_theme_options[general_custom_logo]'
    )
  ));

  $wp_customize->add_setting(
    'zilla_theme_options[general_custom_favicon]',
     array( 'default' => '' )
  );

  $wp_customize->add_control( new WP_Customize_Image_Control(
    $wp_customize,
    'zilla_general_custom_favicon',
    array(
      'label' => __( 'Favicon Upload (16x16 image file)', 'zilla' ),
      'section' => 'zilla_general_options',
      'settings' => 'zilla_theme_options[general_custom_favicon]'
    )
  ));

  $wp_customize->add_setting(
    'zilla_theme_options[general_contact_email]',
    array( 'type' => 'option' )
  );

  $wp_customize->add_control( new WP_Customize_Control(
    $wp_customize,
    'zilla_general_contact_email',
    array(
      'label' => __( 'Contact Form Email Address', 'zilla' ),
      'section' => 'zilla_general_options',
      'settings' => 'zilla_theme_options[general_contact_email]'
    )
  ));

  /* Style Options --- */
  $wp_customize->add_section(
    'zilla_style_options',
    array(
      'title' => __( 'Style Options', 'zilla' ),
      'priority' => 10,
      'capability' => 'edit_theme_options',
      'description' => __('Give your site a custom coat of paint by updating the style options.', 'zilla')
    )
  );

  $wp_customize->add_setting(
    'zilla_theme_options[style_accent_color]',
    array(
      'default' => '#bbbbbb',
      'transport' => 'postMessage'
    )
  );

  $wp_customize->add_control( new WP_Customize_Color_Control(
    $wp_customize,
    'zilla_style_accent_color',
    array(
      'label' => __( 'Accent Color', 'zilla' ),
      'section' => 'zilla_style_options',
      'settings' => 'zilla_theme_options[style_accent_color]'
    )
  ));

  $wp_customize->add_setting( 'zilla_theme_options[style_custom_css]', array('default' => ''));

  $wp_customize->add_control( new Zilla_Customize_Textarea_Control(
    $wp_customize,
    'zilla_style_custom_css',
    array(
      'label' => __( 'Custom CSS', 'zilla' ),
      'section' => 'zilla_style_options',
      'settings' => 'zilla_theme_options[style_custom_css]',
      )
    ));

    /* Social Options --- */
    $wp_customize->add_section(
        'zilla_social_options',
        array(
            'title' => __( 'Social Options', 'zilla' ),
            'priority' => 10,
            'capability' => 'edit_theme_options',
            'description' => __('Add info about your social accounts and they will appear in the footer.', 'zilla')
        )
    );

    $wp_customize->add_setting( 'zilla_theme_options[facebook_url]', array('default' => '') );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'zilla_facebook_url',
        array(
            'label' => __( 'Facebook URL', 'zilla' ),
            'section' => 'zilla_social_options',
            'settings' => 'zilla_theme_options[facebook_url]',
            'priority' => 1
        )
    ));

    $wp_customize->add_setting( 'zilla_theme_options[twitter_url]', array('default' => '') );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'zilla_twitter_url',
        array(
            'label' => __( 'Twitter URL', 'zilla' ),
            'section' => 'zilla_social_options',
            'settings' => 'zilla_theme_options[twitter_url]',
            'priority' => 2
        )
    ));

    $wp_customize->add_setting( 'zilla_theme_options[linkedin_url]', array('default' => '') );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'zilla_linkedin_url',
        array(
            'label' => __( 'LinkedIn URL', 'zilla' ),
            'section' => 'zilla_social_options',
            'settings' => 'zilla_theme_options[linkedin_url]',
            'priority' => 3
        )
    ));

    $wp_customize->add_setting( 'zilla_theme_options[behance_url]', array('default' => '') );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'zilla_behance_url',
        array(
            'label' => __( 'Behance URL', 'zilla' ),
            'section' => 'zilla_social_options',
            'settings' => 'zilla_theme_options[behance_url]',
            'priority' => 4
        )
    ));

    $wp_customize->add_setting( 'zilla_theme_options[dribbble_url]', array('default' => '') );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'zilla_dribbble_url',
        array(
            'label' => __( 'Dribbble URL', 'zilla' ),
            'section' => 'zilla_social_options',
            'settings' => 'zilla_theme_options[dribbble_url]',
            'priority' => 5
        )
    ));

    $wp_customize->add_setting( 'zilla_theme_options[pinterest_url]', array('default' => '') );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'zilla_pinterest_url',
        array(
            'label' => __( 'Pinterest URL', 'zilla' ),
            'section' => 'zilla_social_options',
            'settings' => 'zilla_theme_options[pinterest_url]',
            'priority' => 6
        )
    ));

    $wp_customize->add_setting( 'zilla_theme_options[medium_url]', array('default' => '') );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'zilla_medium_url',
        array(
            'label' => __( 'Medium URL', 'zilla' ),
            'section' => 'zilla_social_options',
            'settings' => 'zilla_theme_options[medium_url]',
            'priority' => 7
        )
    ));

    $wp_customize->add_setting( 'zilla_theme_options[tumblr_url]', array('default' => '') );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'zilla_tumblr_url',
        array(
            'label' => __( 'Tumblr URL', 'zilla' ),
            'section' => 'zilla_social_options',
            'settings' => 'zilla_theme_options[tumblr_url]',
            'priority' => 8
        )
    ));

  if( $wp_customize->is_preview() && ! is_admin() )
    add_action('wp_footer', 'zilla_live_preview', 21);
}

/**
* This outputs the javascript needed to automate the live settings preview.
*
*/
function zilla_live_preview() {
  ?>
    <script type="text/javascript">
    ( function( $ ) {

      wp.customize( 'zilla_theme_options[general_custom_logo]', function( value ) {
        value.bind( function( newval ) {
          console.log(newval);
          $('#logo img').attr('src', newval);
        });
      });

      wp.customize( 'zilla_theme_options[style_accent_color]', function( value ) {
        value.bind( function( newval ) {
          $('#content a').css('color', newval );
        } );
      } );

    } )( jQuery );
  </script>
  <?php
}

/**
* This will output the custom WordPress settings to the live theme's WP head.
*
*/
function header_output() {

  $theme_options = get_theme_mod('zilla_theme_options');

  /* Output the favicon */
  if( !empty($theme_options) && array_key_exists( 'general_custom_favicon', $theme_options ) && $theme_options['general_custom_favicon'] != '' ) {
    echo '<link rel="shortcut icon" href="'. $theme_options['general_custom_favicon'] .'" />' . "\n";
  }
  ?>
  <!--Customizer CSS-->
  <style type="text/css">
       <?php generate_css('#content a', 'color', 'style_accent_color'); ?>
  </style>
  <!--/Customizer CSS-->
  <?php
}

/**
 * This will generate a line of CSS for use in header output. If the setting
 * ($mod_name) has no defined value, the CSS will not be output.
 *
 * @uses get_theme_mod()
 * @param string $selector CSS selector
 * @param string $style The name of the CSS *property* to modify
 * @param string $mod_name The name of the 'theme_mod' option to fetch
 * @param string $prefix Optional. Anything that needs to be output before the CSS property
 * @param string $postfix Optional. Anything that needs to be output after the CSS property
 * @param bool $echo Optional. Whether to print directly to the page (default: true).
 * @return string Returns a single line of CSS with selectors and a property.
 * @since MyTheme 1.0
 */
function generate_css( $selector, $style, $mod_name, $prefix='', $postfix='', $echo=true ) {
  $return = '';
  $mods = get_theme_mod('zilla_theme_options');
  $mod = $mods[$mod_name];
  if ( ! empty( $mod ) ) {
     $return = sprintf('%s { %s:%s; }',
        $selector,
        $style,
        $prefix.$mod.$postfix
     );
     if ( $echo ) {
        echo $return;
     }
  }
  return $return;
}
// Output custom CSS to live site
add_action( 'wp_head' , 'header_output' );
