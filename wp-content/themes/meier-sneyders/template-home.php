<?php
/**
 * Template Name: Home
 *
 * Custom page template for the home page
 *
 * @package Agency
 * @since 1.0
 */

get_header();

$query = zilla_portfolio_featured_query();
?>

	<!--BEGIN #primary .site-main-->
	<div id="primary" class="site-main" role="main">
		<?php if( $query->have_posts() ) : ?>

			<div class="home-slider">
				<?php while( $query->have_posts() ) : $query->the_post(); ?>

					<?php if ( has_post_thumbnail() ) { ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$url = $thumb['0'];
							?>
							<a href="<?php the_permalink(); ?>" style="background-image:url('<?php echo $url; ?>');">
								<h2 class="entry-title"><?php the_title(); ?></h2>
							</a>
						</article>
					<?php } ?>

				<?php endwhile; ?>
			</div>

		<?php endif; ?>
		<?php wp_reset_query(); ?>
	<!--END #primary .site-main-->
	</div>

<?php get_footer(); ?>
