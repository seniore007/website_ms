<?php
/**
 * The template for showing the single portfolio view
 *
 * @package Agency
 * @since 1.0
 */
get_header(); ?>

	<!--BEGIN #primary .site-main-->
	<div id="primary" class="site-main" role="main">

		<?php while (have_posts()) : the_post(); ?>

			<?php get_template_part('content', 'portfolio' ); ?>

			<?php
			    zilla_comments_before();
			    comments_template('', true);
			    zilla_comments_after();
			?>

		<?php endwhile; ?>

	<!--END #primary .site-main-->
	</div>

<?php get_footer(); ?>
