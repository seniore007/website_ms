<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
 	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
 }
 
 	/* Register our styles ------------------------------------------------------*/
	wp_enqueue_style('open-sans', 'http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' );

	/* Register our scripts -----------------------------------------------------*/
	wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr-2.6.2.min.js', '', '2.6.2', false);
	wp_register_script('validation', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js', 'jquery', '1.9', true);
	wp_register_script('superfish', get_template_directory_uri() . '/js/superfish.js', 'jquery', '1.7.4', true);
	wp_register_script('zillaMobileMenu', get_template_directory_uri() . '/js/jquery.zillamobilemenu.min.js', 'jquery', '0.1', true);
	wp_register_script('fitVids', get_template_directory_uri() . '/js/jquery.fitvids.js', 'jquery', '1.0', true);
	wp_register_script('jPlayer', get_template_directory_uri() . '/js/jquery.jplayer.min.js', 'jquery', '2.4', true);
	wp_register_script('isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', 'jquery', '2.0.1', true);
	wp_register_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', 'jquery', '3.1.8', true);
	wp_register_script('slidesjs', get_template_directory_uri() . '/js/jquery.slides.min.js', 'jquery', '3.0.4', true);
	wp_register_script('zilla-custom', get_template_directory_uri() . '/js/jquery.custom.js', array('jquery', 'superfish', 'zillaMobileMenu', 'fitVids', 'jPlayer', 'isotope', 'imagesloaded', 'slidesjs'), '', true);

	/* Enqueue our scripts ------------------------------------------------------*/
	wp_enqueue_script('modernizr');
	wp_enqueue_script('jquery');
	wp_enqueue_script('zillaMobileMenu');
	wp_enqueue_script('superfish');
	wp_enqueue_script('fitVids');
	wp_enqueue_script('jPlayer');
	wp_enqueue_script('zilla-custom');

	/* loads the javascript required for threaded comments ----------------------*/
	if( is_singular() && comments_open() && get_option( 'thread_comments') )
		wp_enqueue_script( 'comment-reply' );

	if( is_page_template('template-contact.php') )
		wp_enqueue_script('validation');