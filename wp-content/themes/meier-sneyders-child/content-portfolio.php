<?php
/**
 * The template to display post content
 *
 * @package Agency
 * @since 1.0
 */

$layout = get_post_meta( $post->ID, '_zilla_agency_portfolio_layout', true );
$layout_class = 'layout-'. ($layout ? $layout : '1col');
zilla_post_before(); ?>
<!--BEGIN .post -->
<article id="post-<?php the_ID(); ?>" <?php post_class($layout_class); ?>>
<?php zilla_post_start(); ?>

	<div class="portfolio-content">

		<?php
	    $portfolio_custom_fields = array();
	    $portfolio_custom_fields['display_gallery'] = get_post_meta( $post->ID, '_tzp_display_gallery', true );
	    $portfolio_custom_fields['display_audio'] = get_post_meta( $post->ID, '_tzp_display_audio', true );
	    $portfolio_custom_fields['display_video'] = get_post_meta( $post->ID, '_tzp_display_video', true );
	    ?>

		<!--BEGIN .entry-header-->
		<header class="entry-header">

			<h1 class="entry-title"><?php the_title(); ?></h1>

			<?php /*if ( has_post_thumbnail() ) { ?>
				<div class="entry-thumbnail">
					<?php the_post_thumbnail( 'portfolio-full' ); ?>
				</div>
			<?php }*/ ?>

		<!--END .entry-header-->
		</header>

		<?php if( $layout == '1col' ){ ?>
		<div class="entry-categories">
			<ul>
				<?php
				$terms = get_the_terms( $post->ID, 'portfolio-type' );
				if( !empty($terms) ) {
					foreach( $terms as $term ) {
						echo '<li><a href="'. get_term_link( $term ) .'">#'. $term->name .'</a></li>';
					}
				}
				?>
			</ul>
		</div>
		<?php } ?>

		<!--BEGIN .entry-content -->
		<div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages: ', 'zilla').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			<?php if( $layout == '2col' ){ ?>
			<div class="entry-categories">
				<ul>
					<?php
					$terms = get_the_terms( $post->ID, 'portfolio-type' );
					if( !empty($terms) ) {
						foreach( $terms as $term ) {
							echo '<li><a href="'. get_term_link( $term ) .'">#'. $term->name .'</a></li>';
						}
					}
					?>
				</ul>
			</div>
			<?php } ?>

	<div class="entry-navigation">
		<span class="next"><?php next_post_link('%link <span class="gt">&gt;</span>'); ?></span>
		<span class="previous"><?php previous_post_link('<span class="lt">&lt;</span> %link'); ?></span>
	</div>
			<?php
			$button_url = get_post_meta( $post->ID, '_zilla_agency_portfolio_button_url', true );
			$button_text = get_post_meta( $post->ID, '_zilla_agency_portfolio_button_text', true );
			if( $button_url && $button_text ){ ?>
			<a href="<?php echo esc_url( $button_url ); ?>" class="btn portfolio-btn"><?php echo esc_attr( $button_text ); ?></a>
			<?php } ?>
		<!--END .entry-content -->
		</div>

	</div>

	<?php
	agency_portfolio_media_feature( $post->ID, $portfolio_custom_fields );
	agency_portfolio_media( $post->ID, $portfolio_custom_fields );
	?>


<?php zilla_post_end(); ?>
<!--END .post-->
</article>
<?php zilla_post_after(); ?>
